#uses "fwDevice/fwDevice.ctl"
#uses "fwConfigs/fwPeriphAddress.ctl"

public void buildAddress(const string &aDpe, const string &aAddress) 
{
  dyn_anytype params;
  dyn_string exc;
  
  params[fwPeriphAddress_TYPE]          = fwPeriphAddress_TYPE_S7;
  params[fwPeriphAddress_DRIVER_NUMBER] = 2;
  params[fwPeriphAddress_ROOT_NAME]     = aAddress; //address
  params[fwPeriphAddress_DIRECTION]     = DPATTR_ADDR_MODE_INPUT_POLL; //DPATTR_ADDR_MODE_INPUT_POLL; //read mode
  params[fwPeriphAddress_DATATYPE]      = 703; //default type convertion. see PVSS help on _address for more options
  params[fwPeriphAddress_ACTIVE]        = TRUE;
  params[fwPeriphAddress_S7_LOWLEVEL]   = FALSE; //or true if you want timestamp to be updated only on value change
  params[FW_PARAMETER_FIELD_INTERVAL]   = 3;
  params[fwPeriphAddress_S7_POLL_GROUP] = "MediumPolling"; //poll group name
  
  fwPeriphAddress_set(aDpe, params, exc);
  
  // exception logging
  int exceptionsAmount = sizeof(exc);
  if(exceptionsAmount > 0)
  {
    for(int i = 0; i < exceptionsAmount; i++)
    {
      DebugN(exc[i]);
    }
  } else
  {
    DebugN("Building address " + aAddress + " finished successfully");
  }
}

public void buildArchive(const string &aDp)
{
  dyn_string exc;
  fwArchive_set(aDp, "RDB-99) EVENT", DPATTR_ARCH_PROC_VALARCH, 0, 0, 0, exc);

  // exception logging
  int exceptionsAmount = sizeof(exc);
  if(exceptionsAmount > 0)
  {
    for(int i = 0; i < exceptionsAmount; i++)
    {
      DebugN(exc[i]);
    }
  } else
  {
    DebugN("Building archiving " + aDp + " finished successfully");
  }
}

public int addDriver(const string &aName, const string &aMng, const string &aParams)
{
  int result;
  string resultText;

  result = fwInstallationManager_append(TRUE, aName, aMng, "manual", 30, 2, 2, aParams);

  switch(result)
  {
    case 1:
      resultText = "OK";
      break;
    case 2:
      resultText = "Already exists";
      break;
    default:
      resultText = "Failed";
      break;
  }

  DebugN("Adding " + aName + " - " + resultText);
  
  return result;
}


