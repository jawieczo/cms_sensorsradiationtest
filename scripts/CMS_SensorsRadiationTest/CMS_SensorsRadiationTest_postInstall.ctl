#uses "CMS_SensorsRadiationTest/configTools.ctl"

void buildRadiationSensorsAddresses()
{
  buildAddress("airTemp1.value", "PIERO_S7300.DB444.DBD56F");
  buildAddress("airTemp2.value", "PIERO_S7300.DB444.DBD60F");
  
  buildAddress("dewPointVaisala1.value", "PIERO_S7300.DB444.DBD64F");
  buildAddress("dewPointVaisala2.value", "PIERO_S7300.DB444.DBD68F");
  
  buildAddress("dewPointWetMirror1.value", "PIERO_S7300.DB444.DBD72F");
  
  buildAddress("flow1.value", "PIERO_S7300.DB444.DBD48F");
  buildAddress("flow2.value", "PIERO_S7300.DB444.DBD52F");

  buildAddress("linPiccoDp1.value", "PIERO_S7300.DB444.DBD80F");
  buildAddress("linPiccoDp2.value", "PIERO_S7300.DB444.DBD84F");
  buildAddress("linPiccoDpError1.value", "PIERO_S7300.DB444.DBD100F");
  buildAddress("linPiccoDpError2.value", "PIERO_S7300.DB444.DBD104F");
  buildAddress("linPiccoRh1.value", "PIERO_S7300.DB444.DBD88F");
  buildAddress("linPiccoRh2.value", "PIERO_S7300.DB444.DBD92F");
  
  buildAddress("pt1000.value", "PIERO_S7300.DB444.DBD96F");  
}

void createDbConnection()
{
  buildArchive("airTemp1.value");  
  buildArchive("airTemp2.value");  
  
  buildArchive("dewPointVaisala1.value");  
  buildArchive("dewPointVaisala2.value");  
  
  buildArchive("dewPointWetMirror1.value");    
  
  buildArchive("flow1.value");    
  buildArchive("flow2.value");    
  
  buildArchive("linPiccoDp1.value");
  buildArchive("linPiccoDp2.value");
  buildArchive("linPiccoDpError1.value");
  buildArchive("linPiccoDpError2.value");
  buildArchive("linPiccoRh1.value");
  buildArchive("linPiccoRh2.value");
  
  buildArchive("pt1000.value");
}

void main()
{
  bool expired = false;
  
  buildRadiationSensorsAddresses();  

  createDbConnection();
  
  fwInstallationManager_command("STOP", "WCCILsim", "-num 2");    
  fwInstallationManager_wait(false, "WCCILsim", 2, 20, expired);    
  
  fwInstallationManager_command("START", "WCCOAs7", "-num 2");   
  fwInstallationManager_wait(true, "WCCOAs7", 2, 20, expired);
  
  fwInstallationManager_command("START", "WCCOArdb", "-num 99");   
  fwInstallationManager_wait(true, "WCCOArdb", 99, 20, expired);
}
