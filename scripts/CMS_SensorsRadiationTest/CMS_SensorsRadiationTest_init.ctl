#uses "CMS_SensorsRadiationTest/configTools.ctl"

void main()
{
  addDriver("Simulation Driver", "WCCILsim", "-num 2");     
  addDriver("Sematic S7 Driver", "WCCOAs7", "-num 2");
  addDriver("RDB Archive Manager", "WCCOArdb", "-num 99");
      
  bool expired = false;

  fwInstallationManager_command("STOP", "WCCOAs7", "-num 2"); 
  fwInstallationManager_wait(false, "WCCOAs7", 2, 20, expired);    

  fwInstallationManager_command("START", "WCCILsim", "-num 2");    
  fwInstallationManager_wait(true, "WCCILsim", 2, 20, expired);   
}
