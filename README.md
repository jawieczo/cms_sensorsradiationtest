# CMS_SensorsRadiationTest

## About
The humidity and temperature are important parameters in the detector environment which have to be constantly measured. 
The main goal of this project is to visualise and archive in Oracle database the readouts for the dewpoint and 
temperature sensors which will be exposed for radiation environment. It will help in choosing the proper sensors which 
will be able to withstand between the maintanance periods when it will be possible to change them to the new ones.
Also the project will help observing the sensors behavior in the time.

## Requirements 
The project requires FwTrending, fwCore, fwRDBArchiverpackage, fwConfigurationDB, fwAccessControl from JCOP framework. 

## Install
When required packages is already installed go to JCOP installation tool, then 
1. Install component and finally run post install script 
2. Put credentials into db via fwRDBArchiver_Overview.xml
3. Edit config file via WinCC OA Console: in the section [ValueArchiveRDB] add row queryFunction = 1